import psycopg2
import json

conn = psycopg2.connect(
    dbname="eiv_estimations",
    user="eiv",
    password="Administrator",
    host="estimator-db.cgtpsfnkku9a.us-west-2.rds.amazonaws.com"
)

cur = conn.cursor()

cur.execute("SELECT * FROM data;")
rows = cur.fetchall()
columns = [desc[0] for desc in cur.description]
objects = [dict(zip(columns, row)) for row in rows]
json_array = json.dumps(objects)
cur.close()
conn.close()

python_arr = json.loads(json_array)
with open("./src/lib/data/data.json", "w") as f:
    json.dump(python_arr, f, separators=(",", ":"))
