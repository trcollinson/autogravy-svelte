import type { Contact } from "$lib/data/contact";

export interface Dealership {
    dealershipId: number;
    dealershipName: string;
    dealershipAddress1: string;
    dealershipAddress2: string;
    dealershipCity: string;
    dealershipState: string;
    dealershipZip: string;
    dealershipContacts: Contact[];
}

export interface Group {
    id?: string;
    groupName: string;
    groupAddress1: string;
    groupAddress2: string;
    groupCity: string;
    groupState: string;
    groupZip: string;
    groupContacts: Contact[];
    dealerships: Dealership[];
    generalManager?: Contact;
}