export interface Contact {
    name: string;
    email: string;
    phone: string;
    type: string;
    primaryContact: boolean;
}

export interface ContactType {
    id: string;
    name: string;
}
