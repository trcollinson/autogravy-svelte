import { writable } from "svelte/store";
import type { ContactType } from "$lib/data/contact";

let dealershipsLocal;
let dealershipListLocal;

export const url = writable("https://3azxt40g32.execute-api.us-west-2.amazonaws.com/Prod");
export const dealerships = writable(dealershipsLocal || JSON.stringify([]));
export const dealershipList = writable(dealershipListLocal || JSON.stringify([]))

export const GroupContactType: ContactType[] = [
    { id: "general-manager", name: "General Manager" },
    { id: "other", name: "Other" },
    { id: "ap-ar", name: "Accounts Payable/Receivable" },
  ];

  export const DealerContactType: ContactType[] = [
    { id: "general-manager", name: "General Manager" },
    { id: "parts-manager", name: "Parts Manager" },
    { id: "service-manager", name: "Service Manager" },
    { id: "other", name: "Other" },
  ];