import { browser } from "$app/environment";
import { writable, derived } from "svelte/store";
import { average } from "$lib/services/average";

let emailLocal;
let adminLocal;
let auth0IDLocal;
let usersLocal;
let agreedLocal;

if (browser) { 
	emailLocal = window.localStorage.email;
	adminLocal = window.localStorage.admin === "true";
	auth0IDLocal = window.localStorage.auth0ID;
	usersLocal = window.localStorage.users;
	agreedLocal = window.localStorage.agreed;
}

export const email = writable(emailLocal || "");
export const isAdmin = writable(adminLocal || false);
export const auth0ID = writable(auth0IDLocal || "");
export const users = writable(usersLocal || JSON.stringify([]));
export const agreedToDisclaimer = writable(agreedLocal || false);
export const url = writable("https://3azxt40g32.execute-api.us-west-2.amazonaws.com/Prod");
if (browser) { window.localStorage.url = url; }

if (browser) {
	email.subscribe((value) => { window.localStorage.email = value });
	isAdmin.subscribe((value) => { window.localStorage.admin = value });
	auth0ID.subscribe((value) => { window.localStorage.auth0ID = value });
	users.subscribe((value) => { window.localStorage.users = value });
	agreedToDisclaimer.subscribe((value) => { window.localStorage.agreed = value });
}

export const state = writable("UT");
export const insurance = writable("Aetna");
export const inNetwork = writable("No");
