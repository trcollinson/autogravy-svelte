import { state, insurance, inNetwork } from "$lib/stores/auth";
import { get } from "svelte/store";
import * as bigData from "$lib/data/data.json"

export async function average() {
    let values;

    try {
        values = bigData.default.filter((record: any) => {
            return (record.state === get(state)
          && record.network === get(inNetwork))
        });
    } catch (error) {
        console.error(error);
    }

    let allData = {
        DTX: 0,
        RTC: 0,
        PHP: 0,
        IOP: 0,
        GOP: 0,
        POC: 0,
        Misc: 0
    };

    let selectedData = {
        DTX: 0,
        RTC: 0,
        PHP: 0,
        IOP: 0,
        GOP: 0,
        POC: 0,
        Misc: 0
    };

    let averageFunc = (array: any): String => {
        let total = 0.0;
        for (let el of array) {
            total += parseFloat(el.allowed);
        }
        if (total === 0) return "";
        return "$" + parseFloat(total / array.length).toFixed(2).toString();
    }

    let priceFunc = (array: any): String => {
        let total = 0.0;
        for (let el of array) {
            total += parseFloat(el.billed);
        }
        return parseFloat(total.toFixed(2)).toLocaleString("en-US");
    }

    let selectedLength = 0;
    let selectedPrice = [];
    for (let el of Object.keys(allData)) {
        let averageAll = averageFunc(
            values.filter((record: any) => { return record.new_loc === el })
        );

        let selectedValues = 
            values.filter((record: any) => {
            return (record.new_loc === el && record.insurance_name === get(insurance))
        });
        selectedLength += selectedValues.length;
        selectedPrice = selectedPrice.concat(selectedValues);
        let averageSelected = averageFunc(
            selectedValues
        );

        allData[el] = averageAll;
        selectedData[el] = averageSelected;
    }
    let price = priceFunc(selectedPrice);

    let data = {
        all: allData,
        selected: selectedData,
        claims: selectedLength,
        price: price
    };
    return data;
}
