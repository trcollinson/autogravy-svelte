import auth0 from "auth0-js";
import { config } from "$lib/config/auth_config";
import { email, auth0ID, isAdmin, users, url } from "$lib/stores/auth";
import { get } from "svelte/store";

async function createClient() {
	let client = new auth0.WebAuth({
		domain: config.domain,
		clientID: config.clientId,
		redirectUri: "http://localhost:5173/claims"
	});

	return client;
}

async function getManagementAccessToken() {
    const params = new URLSearchParams();
    params.append("grant_type", "client_credentials");
    params.append("client_id", config.clientId);
    params.append("client_secret", config.clientSecret);
    params.append("audience", config.audience);
    // const data = {
    //     grant_type: "client_credentials",
    //     client_id: config.clientId,
    //     client_secret: config.clientSecret,
    //     audience: config.audience
    // }
    let res = await fetch(`https://${config.domain}/oauth/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: params
    });
    let resJson = await res.json();
    return resJson.access_token;
}

async function updateUser(auth0ID: string, email: string, password?: string) {
    let token = await getManagementAccessToken();
    console.log(token)

    // let headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", `Bearer ${token}`);
    let headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer ${token}`
    }

    let bodyEmail = {
        "email": email
    };
    let bodyStrEmail = JSON.stringify(bodyEmail);

    let requestOptionsEmail = {
        method: "PATCH",
        headers: headers,
        body: bodyStrEmail
    };
    await fetch(`https://${config.domain}/api/v2/users/auth0|${auth0ID}`, requestOptionsEmail)

    if (password) {
        let bodyPassword = {
            "password": password
        }
        let bodyStrPassword = JSON.stringify(bodyPassword);
        let requestOptionsPassword = {
            method: "PATCH",
            headers: headers,
            body: bodyStrPassword
        };

        await fetch(`https://${config.domain}/api/v2/users/auth0|${auth0ID}`, requestOptionsPassword)
    }

}

async function deleteUserAuth0(auth0ID: string) {
    let token = await getManagementAccessToken();

    let headers = new Headers();
    headers.append("Authorization", `Bearer ${token}`);

    let requestOptions = {
        method: "DELETE",
        headers: headers
    }

    let res = await fetch(`https://${config.domain}/api/v2/users/auth0|${auth0ID}`, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result));
}

async function login(client: any, options: object): Promise<any> {
    options.onRedirecting = async (done) => {
        email.set(options.email);

        let res = await fetch(get(url) + "/users");
        let usersArray = await res.json();
        let userInfo = usersArray.filter((el) => {
            return el.email === options.email;
        })[0];

        if (userInfo.role === "admin") { 
            isAdmin.set(true); 
            users.set(JSON.stringify(usersArray));
        }
        auth0ID.set(userInfo.auth0ID);

        done();
    };

	return new Promise((resolve, reject) => {
		client.login(options, async (err: object) => {
			if (err) {
				email.set("");
				isAdmin.set(false);
				auth0ID.set("");
                users.set(JSON.stringify([]))
				resolve(err);
			}
		});
	});
}

async function signup(client: any, options: object): Promise<any> {
	return new Promise((resolve, reject) => {
		client.signup(options, (err, response) => {
			if (err) {
				reject(err);
			} else {
				resolve(response);
			}
		});
	});
}

function logout(options: object) {
	return auth0.logout(options);
}

const auth = {
	createClient,
    updateUser,
    deleteUserAuth0,
	login,
	signup,
	logout
}

export {
	auth
};
