import { claims } from "$lib/stores/claims";

export function load({ params }: any):any {

    const claimId = params.slug;

    return {
        claimInfo: claims.find(i => i.claim === claimId)
    }
}