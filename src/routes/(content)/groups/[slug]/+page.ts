import { get } from "svelte/store";
import { url, dealerships } from "$lib/stores/dealerships";
import type { Group } from "$lib/data/group"

export async function load({ params }: any) {
    let groupInfo;
    const groupId: string = params.slug;
    let groups: Group[] = JSON.parse(get(dealerships))
    if (groups.length > 0) {
        groupInfo = groups.filter((el) => {
            return el.id === groupId;
        })[0];
    } else {
        const response = await fetch(`${get(url)}/dealerships/${groupId}`);
        groupInfo = await response.json();
    }

	return {
		...groupInfo
	};
}
