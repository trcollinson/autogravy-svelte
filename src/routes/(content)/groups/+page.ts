import { get } from "svelte/store";
import { dealerships, url } from "$lib/stores/dealerships";
import type { Group } from "$lib/data/group"

const getGeneralManager = item => {
    let generalManager = item.groupContacts.find((contact) => contact.type === "general-manager");
    item["generalManager"] = generalManager
    let newitem = item.dealerships.map(getDealerPartsManager)
    return item
}

const getDealerPartsManager = item => {
    let generalManager = item.dealershipContacts.find((contact) => contact.type === "parts-manager");
    item["partsManager"] = generalManager
    return item
}

// let groups: Group[] = dealershipsRes.map(getGeneralManager)


export const load = async ({ fetch }:any) => {
    let dealershipsRes: Group[] = JSON.parse(get(dealerships));

    if (dealershipsRes.length < 1) {
        const response = await fetch(`${get(url)}/dealerships`, {
            method: "GET",
        });
        dealershipsRes = await response.json();
        dealerships.set(JSON.stringify(dealershipsRes));
    }

    let groups: Group[] = dealershipsRes.map(getGeneralManager)

    return {
        groups
    };
}