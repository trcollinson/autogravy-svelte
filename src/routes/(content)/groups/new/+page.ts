import { get } from "svelte/store";
import { isAdmin } from "$lib/stores/auth";
import { redirect } from "@sveltejs/kit";

export const load = async () => {
	if (!get(isAdmin)) { throw redirect(308, "/claims"); }
}
