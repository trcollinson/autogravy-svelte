import { get } from "svelte/store";
import { url, users } from "$lib/stores/auth";
import { dealershipList } from "$lib/stores/dealerships";

export async function load({ params }: any) {
    let dealershipListRes;
    let userInfo;
    const userID = params.slug;
    if (get(users)) {
        let usersInfo = JSON.parse(get(users));
        userInfo = usersInfo.filter((el) => {
            return el.auth0ID === userID;
        })[0];
    } else {
        const response = await fetch(`${get(url)}/users/${userID}`);
        userInfo = await response.json();
    }

	if (JSON.parse(get(dealershipList)).length > 0) {
		dealershipListRes = JSON.parse(get(dealershipList));
	} else {
		const response = await fetch(`${get(url)}/dealerships?list=reduced`, {
			method: "GET",
		});
		dealershipListRes = await response.json();
		dealershipList.set(JSON.stringify(dealershipListRes));
	}

	return {
		userInfo,
        dealershipListRes,
		userID: userID
	};
}
