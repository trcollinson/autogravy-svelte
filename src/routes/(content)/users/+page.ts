import { get } from "svelte/store";
import { users, url } from "$lib/stores/auth";

export const load = async ({ fetch }: any) => {
    let usersRes;
    if (JSON.parse(get(users))) {
        usersRes = JSON.parse(get(users));
    } else {
        const response = await fetch(`${get(url)}/users`, {
            method: "GET",
        });
        usersRes = await response.json();
        users.set(JSON.stringify(usersRes));
    }

	return {
		usersRes
	};
}
