import { get } from "svelte/store";
import { isAdmin } from "$lib/stores/auth";
import { redirect } from "@sveltejs/kit";
import { dealershipList, url } from "$lib/stores/dealerships";

export const load = async () => {
	if (!get(isAdmin)) { throw redirect(308, "/claims"); }

	let dealershipListRes;
	if (JSON.parse(get(dealershipList)).length > 0) {
		dealershipListRes = JSON.parse(get(dealershipList));
	} else {
		// const response = await fetch(`${get(url)}/dealerships?list=reduced`, {
		const response = await fetch(`${get(url)}/dealerships`, {
			method: "GET",
		});
		dealershipListRes = await response.json();
		dealershipList.set(JSON.stringify(dealershipListRes));
	}

	return {
		dealershipListRes
	};
}
