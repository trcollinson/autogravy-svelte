import { redirect } from "@sveltejs/kit";
import { email, isAdmin, auth0ID, users, agreedToDisclaimer } from "$lib/stores/auth";

export function load() {
	email.set("");
	isAdmin.set(false);
	auth0ID.set("");
	users.set(JSON.stringify([]));
	agreedToDisclaimer.set(false);

	// let options = {
	// 	returnTo: "http://localhost:5173",
	// 	clientID: config.clientId
	// };
	// auth.logout(options);
	throw redirect(308, "/login");
}
